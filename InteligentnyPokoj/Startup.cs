﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InteligentnyPokoj.Startup))]
namespace InteligentnyPokoj
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
